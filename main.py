import argparse
from pathlib import Path
import pandas


def predict(inputs: Path):    
    inputs = list(inputs)
    
    # your algorithm goes here
    
    # delete and rewrite values with your predictions
    values = {
        "image_uid":[Path(input).stem for input in inputs],
        "level_0": ["Animalia"] * len(inputs),
        "level_0_probability": [1.0] * len(inputs)
    }

    return mapping_to_csv(values)

def mapping_to_csv(values: dict[str: any]):
    columns = ["image_uid","level_0","level_0_probability","level_1","level_1_probability","level_2","level_2_probability","level_3","level_3_probability","level_4","level_4_probability","level_5","level_5_probability"]

    return pandas.DataFrame(columns=columns, data=values)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "input_path", help="Path to input images"
    )

    parser.add_argument(
        "output_path", help="Path to output csv"
    )

    args = parser.parse_args()

    predict(Path(args.input_path).glob("*.jpg")).to_csv(args.output_path, index=False)
